const { encode } = require('./encode')
const { decode, decodeText } = require('./decode')

module.exports = { encode, decode, decodeText }
